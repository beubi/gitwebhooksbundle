<?php

namespace Beubi\GitWebHooksBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Beubi\GitWebHooksBundle\WebhookRequest\WebhookFactory;

/**
 * Class GitWebhookEvent
 *
 * @package    BeubiGitWebhookBundle
 * @subpackage Event
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class GitWebhookEvent extends Event implements GitWebhookEventInterface
{
    private $eventName;
    private $response;

    /**
     *
     * @param string $eventName
     * @param array  $response
     */
    public function __construct($eventName, array $response)
    {
        $this->eventName = $eventName;
        $this->response = $response;
    }

    /**
     *
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    /**
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return mixed|null
     */
    public function getWebhook()
    {
        $webhookFactory = new WebhookFactory($this->eventName, $this->response);

        return $webhookFactory->create();
    }
}

<?php
namespace Beubi\GitWebHooksBundle\Event;

/**
 * Class GitWebhookEvents
 *
 *
 * @package    GitWebhookBundle
 * @subpackage Event
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class GitWebhookEvents
{
    const WEBHOOK_RECEIVED = 'beubi_git_webhook.webhook_received';
}

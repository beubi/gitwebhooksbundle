<?php

namespace Beubi\GitWebHooksBundle\Event;

/**
 * interface GitWebhookEventInterface
 *
 * @package    BeubiGitWebHooksBundle
 * @subpackage Event
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
interface GitWebhookEventInterface
{
    /**
     * @return string
     */
    public function getEventName();

    /**
     * @return string
     */
    public function getResponse();

    /**
     * @return \Beubi\GitWebHooksBundle\WebhookRequest\WebhookRequestInterface
     */
    public function getWebhook();
}

<?php

namespace Beubi\GitWebHooksBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('beubi_git_web_hooks');

        $rootNode
          ->children()
                ->arrayNode('monolog')
                    ->children()
                        ->scalarNode('channel')
                            ->defaultValue('BeubiGitWebHooksRequest')
                        ->end()
                    ->end()
               ->end()
            ->end();

        return $treeBuilder;
    }
}

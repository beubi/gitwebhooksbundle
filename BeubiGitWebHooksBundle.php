<?php

namespace Beubi\GitWebHooksBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class BeubiGitWebHooksBundle
 *
 * @package    GitWebhookInterface
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class BeubiGitWebHooksBundle extends Bundle
{
}

<?php
namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Interface PullRequestInterface
 *
 * @package    GitWebhookInterface
 * @subpackage WebhookRequest
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
interface PullRequestInterface extends WebhookRequestInterface
{
    /**
     * @return string
     */
    public function getSourceBranch();

    /**
     * @return string
     */
    public function getDestinationBranch();

    /**
     * @return string
     */
    public function getLink();
    /**
     * @return string
     */
    public function getIssue();
}

<?php
namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Class WebhookFactory
 *
 * @package    GitWebhookInterface
 * @subpackage WebhookRequest
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class WebhookFactory
{

    private $data;
    private $eventName;

    /**
     * Constructor
     *
     * @param string $eventName Webhook event name
     * @param array  $data      Webhook json decoded body
     */
    public function __construct($eventName, array $data)
    {
        $this->data = $data;
        $this->eventName = $eventName;
    }

    /**
     * @return mixed|null
     */
    public function create()
    {
        if (isset($this->data['before']) && isset($this->data['after']) && isset($this->data['repository'])) {
            //Github webhook
            return new GithubWebhook($this->data);
        } elseif (isset($this->data['canon_url']) && isset($this->data['repository']['absolute_url'])) {
            //Bitbucket webhook
            return new BitbucketWebhook($this->data);
        } elseif ($this->eventName == 'pullrequest:created'
            || $this->eventName == 'pullrequest:rejected'
        ) {
            // From configured Bitbucket webhook
            return new BitbucketWebhookPullRequest($this->data);
        } elseif (isset($this->data['pull_request'])) {
            //Github Pull Request created webhook
            return new GithubWebhookPullRequest($this->data);
        } elseif (isset($this->data['push'])) {
            //Github Pull Request created webhook
            return new BitbucketWebhookPush($this->data);
        }
    }
}

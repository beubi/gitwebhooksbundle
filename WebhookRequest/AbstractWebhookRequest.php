<?php

namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Abstract class AbstractWebhookRequest
 *
 * @package    BeubiGitWebhookBundle
 * @subpackage WebhookRequest
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
abstract class AbstractWebhookRequest implements WebhookRequestInterface
{
    protected $content;

    /**
     * class constructor
     *
     * @param array $data Webhook json decoded body.
     */
    public function __construct(array $data)
    {
        $this->content = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->content;
    }

    /**
     * Get the source control service name
     *
     * @return string
     */
    public function getSourceControlService()
    {
        $hostname = $this->getHostname();

        $reversed = strrev($hostname);

        $p = strpos($reversed, '.');

        return strrev(substr($reversed, $p + 1));
    }
}

<?php

namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Class GithubWebhookPullRequest
 * https://developer.github.com/v3/activity/events/types/#pullrequestevent
 *
 * @package Beubi\GitWebHooksBundle\WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class GithubWebhookPullRequest extends AbstractWebhookRequest implements PullRequestInterface
{
    /**
     * Get the repository Fullname
     *
     * @return string
     */
    public function getRepositoryFullName()
    {
        return $this->content['pull_request']['head']['repo']['full_name'];
    }

    /**
     * Get the repository URL
     *
     * @return string
     */
    public function getHostname()
    {
        return 'github.com';
    }

    /**
     * @return string
     */
    public function getSourceBranch()
    {
        return $this->content['pull_request']['head']['ref'];
    }

    /**
     * @return string
     */
    public function getDestinationBranch()
    {
        return $this->content['pull_request']['base']['ref'];
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->content['pull_request']['html_url'];
    }
    /**
     * @return string
     */
    public function getIssue()
    {
        return $this->content['number'];
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->content['action'];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->content['pull_request']['title'];
    }
}

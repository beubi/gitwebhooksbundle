<?php

namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Class BitbucketWebhook
 * https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management
 *
 * @package Beubi\GitWebHooksBundle\WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class BitbucketWebhookPush extends AbstractWebhookRequest implements PushInterface
{
    /**
     * Get the repository Fullname
     *
     * @return string
     */
    public function getRepositoryFullName()
    {
        return $this->content['repository']['full_name'];
    }

    /**
     * Get the repository URL
     *
     * @return string
     */
    public function getHostname()
    {
        return 'bitbucket.org';
    }

    /**
     * @return array
     */
    public function getBranches()
    {
        $branches = array();
        foreach ($this->content['push']['changes'] as $changeSet) {
            if ($changeSet['new'] != null) {
                $branches[] = $changeSet['new']['name'];
            }
        }

        return $branches;
    }
}

<?php

namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Class to manage webhooks from github service
 * https://developer.github.com/v3/activity/events/types/#pushevent
 *
 * @package Beubi\GitWebHooksBundle\WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class GithubWebhook extends AbstractWebhookRequest
{
    /**
     * Get the repository name
     *
     * @return string
     */
    public function getRepositoryFullName()
    {
        return $this->content['repository']['full_name'];
    }

    /**
     * Get the repository URL hostname
     *
     * @return string|false The hostname or false if the URL is malformed
     */
    public function getHostname()
    {
        return parse_url($this->content['repository']['url'], PHP_URL_HOST);
    }
}

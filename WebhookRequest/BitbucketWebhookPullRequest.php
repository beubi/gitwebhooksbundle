<?php

namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Class BitbucketWebhook
 * https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management
 *
 * @package BeubiGitWebHooksBundle
 * @subpackage WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class BitbucketWebhookPullRequest extends AbstractWebhookRequest implements PullRequestInterface
{
    /**
     * Get the repository Fullname
     *
     * @return string
     */
    public function getRepositoryFullName()
    {
        return $this->content['pullrequest']['source']['repository']['full_name'];
    }

    /**
     * Get the repository URL
     *
     * @return string
     */
    public function getHostname()
    {
        return 'bitbucket.org';
    }

    /**
     * @return string
     */
    public function getSourceBranch()
    {
        return $this->content['pullrequest']['source']['branch']['name'];
    }

    /**
     * @return string
     */
    public function getDestinationBranch()
    {
        return $this->content['pullrequest']['destination']['branch']['name'];
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->content['pullrequest']['links']['html']['href'];
    }

    /**
     * @return string
     */
    public function getIssue()
    {
        return $this->content['pullrequest']['id'];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->content['pullrequest']['title'];
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->content['pullrequest']['state'];
    }

}

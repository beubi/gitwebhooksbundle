<?php

namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Class BitbucketWebhook
 * https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management
 *
 * @package Beubi\GitWebHooksBundle\WebhookRequest
 * @author Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class BitbucketWebhook extends AbstractWebhookRequest
{
    /**
     * Get the repository Fullname
     *
     * @return string
     */
    public function getRepositoryFullName()
    {
        $nameParts = pathinfo(substr($this->content['repository']['absolute_url'], 1));
        return $nameParts['dirname'].'/'.$nameParts['basename'];
    }

    /**
     * Get the repository URL hostname
     *
     * @return string|false The hostname or false if the URL is malformed
     */
    public function getHostname()
    {
        return parse_url($this->content['canon_url'], PHP_URL_HOST);
    }
}

<?php
namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Interface PushInterface
 *
 * @package    GitWebhookInterface
 * @subpackage WebhookRequest
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
interface PushInterface extends WebhookRequestInterface
{
    /**
     * @return string
     */
    public function getBranches();
}

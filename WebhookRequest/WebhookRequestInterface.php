<?php
namespace Beubi\GitWebHooksBundle\WebhookRequest;

/**
 * Interface WebhookRequestInterface
 *
 * @package    GitWebhookInterface
 * @subpackage WebhookRequest
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
interface WebhookRequestInterface
{
    /**
     * Get the repository full name
     *
     * @return string
     */
    public function getRepositoryFullName();

    /**
     * Get the hostname
     *
     * @return string
     */
    public function getHostname();

    /**
     * Get the original webhook data
     *
     * @return array
     */
    public function getData();

        /**
     * Get the source control service name
     *
     * @return string
     */
    public function getSourceControlService();
}

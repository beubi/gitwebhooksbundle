[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/beubi/gitwebhooksbundle/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/beubi/gitwebhooksbundle/?branch=master)  [![Build Status](https://scrutinizer-ci.com/b/beubi/gitwebhooksbundle/badges/build.png?b=master)](https://scrutinizer-ci.com/b/beubi/gitwebhooksbundle/build-status/master)

This is a decouple bundle, please don't use any project dependency inside this bundle.

Useful link about standalone Symfony2 Bundle
http://gnugat.github.io/2014/10/29/sf2-bundle-standalone.html

# Running Commands 

As you usually do with app/console

```
#!sh

php Tests/Functional/app/console.php
```

# Manually Testing Pages

##### Start the server:

```
#!sh

php Tests/Functional/app/console.php server:run -d Tests/Functional/app
```

# Running Tests 

```
#!sh

./vendor/bin/phpunit
```

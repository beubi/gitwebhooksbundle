<?php

namespace Beubi\GitWebHooksBundle\Controller;

use Beubi\GitWebHooksBundle\Event\GitWebhookEvent;
use Beubi\GitWebHooksBundle\Event\GitWebhookEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WebhookController
 *
 * @package    BeubiGitWebhooksBundle
 * @subpackage Controller
 * @author     Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class WebhookController extends Controller
{
    /**
     *
     * @param Request $request Http request
     * @Route("/api/webhook")
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        try {
            // 'payload' POST parameter is still used by Github, but no longer by Bitbucket
            $payload = json_decode($request->request->get('payload'), true);
            if (!$payload && $request->headers->get('Content-Type') === 'application/json') {
                $payload = json_decode($request->getContent(), true);
            }

            if (is_array($payload) && !is_null($request->headers->get('X-Event-Key'))) {
                $event = $request->headers->get('X-Event-Key');
            } else {
                $event = 'unknown';
            }
            $this->get('monolog.logger.'.$this->getParameter('beubi_git_web_hooks.monolog.channel'))
              ->info('Webhook received | Event: '.$event.' | Payload: '.json_encode($payload));

            $this->container->get('event_dispatcher')->dispatch(
                GitWebhookEvents::WEBHOOK_RECEIVED,
                new GitWebhookEvent($event, $payload)
            );
        } catch (\Exception $e) {
            $this->get('monolog.logger.'.$this->getParameter('beubi_git_web_hooks.monolog.channel'))
              ->error('Webhook request exception | Message: '.$e->getMessage());
        }

        return new Response('ok', Response::HTTP_OK);
    }
}

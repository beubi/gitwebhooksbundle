<?php

namespace Beubi\GitWebHooksBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WebhookControllerTest extends WebTestCase
{
    private $client;

    protected function setUp()
    {
        $this->client = static::createClient();
    }

    public function testBitbucketRequestIndex()
    {
        $this->simulateRequestTest('bitbucket_pull_request.json', array('HTTP_X-Event-Key' => 'pullrequest:created'));
    }

    public function testGithubRequestIndex()
    {
        $this->simulateRequestTest('github_PullRequestEvent_opened.json');
    }

    public function testGithubPRClose()
    {
        $this->simulateRequestTest('github_PullRequestEvent_closed.json');
    }

    public function testBitbucketPushIndex()
    {
        $this->simulateRequestTest('bitbucket_Push.json');
    }

    /**
     * @param string $jsonFixturesFilename
     */
    private function simulateRequestTest($jsonFixturesFilename, array $headers = array())
    {
        $content = $this->getJsonFixture($jsonFixturesFilename);
        
        $this->client->request('POST', '/api/webhook', array('payload' => $content), array(), $headers);

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('ok', $this->client->getResponse()->getContent());
    }

    /**
     * Return the fixtures
     *
     * @return string
     */
    private function getJsonFixture($filename)
    {
        $fixturesPath = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'jsonFixtures');
        $fixture = file_get_contents($fixturesPath.DIRECTORY_SEPARATOR.$filename);

        return $fixture;
    }
}

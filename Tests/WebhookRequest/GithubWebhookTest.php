<?php

namespace Beubi\GitWebHooksBundle\Tests\WebhookRequest;

use Beubi\GitWebHooksBundle\WebhookRequest\GithubWebhook;

/**
 * Class GithubWebhookTest
 *
 * @package Beubi\GitWebHooksBundle\Tests\WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class GithubWebhookTest extends \PHPUnit_Framework_TestCase {

    /** @var GithubWebhook */
    private $githubWebhook;

    protected function setUp()
    {
        $data = array(
            'before' => '4d2ab4e76d0d405d17d1a0f2b8a6071394e3ab40',
            'after' => '7700ca29dd050d9adacc0803f866d9b539513535',
            'repository' => array(
                'name' => 'public-repo',
                'full_name' => 'baxterthehacker/public-repo',
                'url' => 'https://github.com/baxterthehacker/public-repo')
        );

        $this->githubWebhook = new GithubWebhook($data);
    }

    public function testGetRepositoryFullName()
    {
        $this->assertEquals('baxterthehacker/public-repo', $this->githubWebhook->getRepositoryFullName());
    }

    public function testGetHostname()
    {
        $this->assertEquals('github.com', $this->githubWebhook->getHostname());
    }
}

<?php

namespace Beubi\GitWebHooksBundle\Tests\WebhookRequest;

use Beubi\GitWebHooksBundle\WebhookRequest\BitbucketWebhookPullRequest;

/**
 * Class BitbucketWebhookPullRequestTest
 *
 * @package Beubi\GitWebHooksBundle\Tests\WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class BitbucketWebhookPullRequestTest extends \PHPUnit_Framework_TestCase
{
    /** @var BitbucketWebhookPullRequest */
    private $bitbucketWebhookPullRequest;
    private $data;

    protected function setUp()
    {
        $this->data = array(
            'pullrequest' => array(
                'destination' => array(
                    'branch' => array('name' => 'staging')
                ),
                'links' => array(
                    'decline' => array(
                        'href' => 'https://api.bitbucket.org/2.0/repositories/evzijst/bitbucket2/pullrequests/24/decline'
                    ),
                    'html' => array(
                        'href' => 'https://bitbucket.org/evzijst/bitbucket2/pull-request/24'
                    )),
                'source' => array(
                    'repository' => array(
                        'name' => 'bitbucket2',
                        'full_name' => 'evzijst/bitbucket2'
                    ),
                    'branch' => array('name' => 'mfrauenholtz/inbox')
                )
            )
        );

        $this->bitbucketWebhookPullRequest = new BitbucketWebhookPullRequest($this->data);
    }

    public function testInterface()
    {
        $this->assertInstanceOf('Beubi\GitWebHooksBundle\WebhookRequest\PullRequestInterface', $this->bitbucketWebhookPullRequest);
    }
    public function testGetRepositoryFullName()
    {
        $this->assertEquals('evzijst/bitbucket2', $this->bitbucketWebhookPullRequest->getRepositoryFullName());
    }

    public function testGetHostname()
    {
        $this->assertEquals('bitbucket.org', $this->bitbucketWebhookPullRequest->getHostname());
    }
    public function testGetDestinationBranch()
    {
        $this->assertEquals('staging', $this->bitbucketWebhookPullRequest->getDestinationBranch());
    }

    public function testGetSourceBranch()
    {
        $this->assertEquals('mfrauenholtz/inbox', $this->bitbucketWebhookPullRequest->getSourceBranch());
    }

    public function testGetData()
    {
        $this->assertEquals($this->data, $this->bitbucketWebhookPullRequest->getData());
    }

    public function testGetLink()
    {
        $this->assertEquals(
            'https://bitbucket.org/evzijst/bitbucket2/pull-request/24',
            $this->bitbucketWebhookPullRequest->getLink()
        );
    }
}

<?php

namespace Beubi\GitWebHooksBundle\Tests\WebhookRequest;


use Beubi\GitWebHooksBundle\WebhookRequest\WebhookFactory;

/**
 * Class WebhookFactoryTest
 *
 * @package Beubi\GitWebHooksBundle\Tests\WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class WebhookFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testCreateGithubWebhookObject()
    {
        $data = array(
            'before' => '4d2ab4e76d0d405d17d1a0f2b8a6071394e3ab40',
            'after' => '7700ca29dd050d9adacc0803f866d9b539513535',
            'repository' => array(
                'name' => 'public-repo',
                'full_name' => 'baxterthehacker/public-repo',
                'url' => 'https://github.com/baxterthehacker/public-repo')
        );
        $factory = new WebhookFactory('unknown', $data);
        $object = $factory->create();

        $this->assertInstanceOf('Beubi\GitWebHooksBundle\WebhookRequest\WebhookRequestInterface', $object);
        $this->assertInstanceOf('Beubi\GitWebHooksBundle\WebhookRequest\GithubWebhook', $object);
    }

    public function testCreateBitbucketWebhookObject()
    {
        $data = array(
            'canon_url' => 'https://bitbucket.org',
            'repository' => array(
                'name' => 'Project X',
                'absolute_url' => '/marcus/project-x/')
        );
        $factory = new WebhookFactory('pullrequest:created', $data);
        $object = $factory->create();

        $this->assertInstanceOf('Beubi\GitWebHooksBundle\WebhookRequest\WebhookRequestInterface', $object);
        $this->assertInstanceOf('Beubi\GitWebHooksBundle\WebhookRequest\BitbucketWebhook', $object);
    }
}

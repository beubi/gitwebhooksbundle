<?php

namespace Beubi\GitWebHooksBundle\Tests\WebhookRequest;

use Beubi\GitWebHooksBundle\WebhookRequest\BitbucketWebhook;

/**
 * Class GithubWebhookTest
 *
 * @package Beubi\GitWebHooksBundle\Tests\WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class BitbucketWebhookTest extends \PHPUnit_Framework_TestCase {

    /** @var BitbucketWebhook */
    private $bitbucketWebhook;

    protected function setUp()
    {
        $data = array(
            'canon_url' => 'https://bitbucket.org',
            'repository' => array(
                'name' => 'Project X',
                'absolute_url' => '/marcus/project-x/')
        );

        $this->bitbucketWebhook = new BitbucketWebhook($data);
    }

    public function testGetRepositoryFullName()
    {
        $this->assertEquals('marcus/project-x', $this->bitbucketWebhook->getRepositoryFullName());
    }

    public function testGetHostname()
    {
        $this->assertEquals('bitbucket.org', $this->bitbucketWebhook->getHostname());
    }
}

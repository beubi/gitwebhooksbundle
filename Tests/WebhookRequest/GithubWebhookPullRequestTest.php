<?php

namespace Beubi\GitWebHooksBundle\Tests\WebhookRequest;

use Beubi\GitWebHooksBundle\WebhookRequest\GithubWebhookPullRequest;


/**
 * Class GithubWebhookPullRequestCreatedTest
 *
 * @package Beubi\GitWebHooksBundle\Tests\WebhookRequest
 * @author  Ubiprism Lda. / be.ubi <contact@beubi.com>
 */
class GithubWebhookPullRequestCreatedTest extends \PHPUnit_Framework_TestCase
{
    /** @var  GithubWebhookPullRequest */
    private $githubWebhookPullRequest;
    private $data;

    protected function setUp()
    {
        $this->data = array(
            'action' => 'opened',
            'pull_request' => array(
                'url' => "https://api.github.com/repos/baxterthehacker/public-repo/pulls/50",
                'html_url' => 'https://github.com/baxterthehacker/public-repo/pull/50',
                'head' => array(
                    'ref' => 'mfrauenholtz/inbox',
                    'repo' => array(
                        'full_name' => 'baxterthehacker/public-repo',
                        )),
                    'base' => array(
                        'ref' => 'master'
                )
            ),
        );

        $this->githubWebhookPullRequest = new GithubWebhookPullRequest($this->data);
    }
    public function testInterface()
    {
        $this->assertInstanceOf('Beubi\GitWebHooksBundle\WebhookRequest\PullRequestInterface', $this->githubWebhookPullRequest);
    }
    public function testGetRepositoryFullName()
    {
        $this->assertEquals('baxterthehacker/public-repo', $this->githubWebhookPullRequest->getRepositoryFullName());
    }

    public function testGetHostname()
    {
        $this->assertEquals('github.com', $this->githubWebhookPullRequest->getHostname());
    }
    public function testGetDestinationBranch()
    {
        $this->assertEquals('master', $this->githubWebhookPullRequest->getDestinationBranch());
    }

    public function testGetSourceBranch()
    {
        $this->assertEquals('mfrauenholtz/inbox', $this->githubWebhookPullRequest->getSourceBranch());
    }

    public function testGetData()
    {
        $this->assertEquals($this->data, $this->githubWebhookPullRequest->getData());
    }

    public function testGetAction()
    {
        $this->assertEquals(
            'https://github.com/baxterthehacker/public-repo/pull/50',
            $this->githubWebhookPullRequest->getLink()
        );
    }

    public function testGetLink()
    {
        $this->assertEquals('opened', $this->githubWebhookPullRequest->getAction());
    }
}
